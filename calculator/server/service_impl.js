const { SumResponse } = require('../proto/sum_pb');
const { PrimeResponse } = require('../proto/prime_pb');
const { AvgResponse } = require('../proto/avg_pb');
const { MaxResponse } = require('../proto/max_pb');
const { DATA, END } = require('../../constants');

exports.sum = (call, callback) => {
    console.log('Sum was invoked');
    const res = new SumResponse().setSumResult(
        call.request.getFirstNumber() + call.request.getSecondNumber()
    );

    callback(null, res);
};

exports.primes = (call, _) => {
    console.log('Primes was invoked');
    const res = new PrimeResponse();
    let number = call.request.getNumber();
    let divisor = 2;

    while (number > 1) {
        if (number % divisor === 0) {
            res.setPrimeResult(divisor);
            call.write(res);
            number /= divisor;
        } else {
            ++divisor;
        }
    }

    call.end();
};

exports.avg = (call, callback) => {
    console.log('Avg was invoked');
    let sum = 0;
    let count = 0;

    call.on('data', (req) => {
        sum += req.getNumber();
        count++;
    });

    call.on('end', () => {
        const res = new AvgResponse().setAvgResult(sum / count);
        callback(null, res);
    });
};

exports.max = (call, _) => {
    console.log('Max was invoked');
    let max = 0;

    call.on(DATA, (req) => {
        console.log('Received request: ' + req);
        const number = req.getNumber();

        if (number > max) {
            const res = new MaxResponse().setMaxResult(number);

            console.log('Sending response: ' + res);
            call.write(res);
            max = number;
        }
    });

    call.on(END, () => call.end());
};
