// const fs = require('fs');
const grpc = require('@grpc/grpc-js');
const { CalculatorServiceClient } = require('../proto/calculator_grpc_pb');
const { SumRequest } = require('../proto/sum_pb');
const { PrimeRequest } = require('../proto/prime_pb');
const { AvgRequest } = require('../proto/avg_pb');
const { MaxRequest } = require('../proto/max_pb');
const { DATA } = require('../../constants');

function doSum(client) {
    console.log('doSum was invoked');
    const req = new SumRequest().setFirstNumber(3).setSecondNumber(100);

    client.sum(req, (err, res) => {
        if (err) {
            console.log(err);
            return;
        }

        console.log(`Sum: ${res.getSumResult()}`);
    });
}

function doPrimes(client) {
    console.log('doPrimes was invoked');
    const req = new PrimeRequest().setNumber(120);
    const call = client.primes(req);

    call.on(DATA, (res) => {
        console.log(`Primes: ${res.getPrimeResult()}`);
    });
}

function doAvg(client) {
    console.log('doAvg was invoked');
    const numbers = [...Array(11).keys()].slice(1);
    const call = client.avg((err, res) => {
        if (err) {
            return console.error(err);
        }

        console.log(`Avg: ${res.getAvgResult()}`);
    });

    numbers
        .map((number) => {
            return new AvgRequest().setNumber(number);
        })
        .forEach((req) => call.write(req));

    call.end();
}

function doMax(client) {
    console.log('doMax was invoked');
    const numbers = [4, 7, 2, 19, 4, 6, 32];
    const call = client.max();

    call.on(DATA, (res) => {
        console.log(`Max: ${res.getMaxResult()}`);
    });

    numbers
        .map((number) => {
            return new MaxRequest().setNumber(number);
        })
        .forEach((req) => call.write(req));

    call.end();
}

function main() {
    // const tls = true;
    // let creds;

    // if (tls) {
    //   const rootCert = fs.readFileSync('./ssl/ca.crt');

    //   creds = grpc.ChannelCredentials.createSsl(rootCert);
    // } else {
    const creds = grpc.ChannelCredentials.createInsecure();
    // }

    const client = new CalculatorServiceClient('127.0.0.1:50052', creds);

    // doSum(client);
    doPrimes(client);
    // doAvg(client);
    // doMax(client);
    // doSqrt(client, 25);
    // doSqrt(client, -1);
    client.close();
}

main();
